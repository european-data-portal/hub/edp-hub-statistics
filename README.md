edp statistics
=======================

There are two ways to start this application. 

**1. Using Docker**

Before the build process, `docker-compose` has to be completed with all environment variables.
A list can be found at the end of this document. 

Build simple the Docker images with `docker-compose`:

`docker-compose up --build`
    
**2. As a Python Application**

To start the service as a Python application, a **Python 3.5 or higher** installation is required. 
It is recommended to use *virtualenv* ([User Guide](https://virtualenv.pypa.io/en/latest/userguide/)). 
You can download it via *PyPi* ([Installation Guide](https://virtualenv.pypa.io/en/latest/installation/)):

    pip install virtualenv
    
Change in the programm directory.

    cd edp-hub-statistics
    
Run virtualenv with the name `env`.

    virtualenv env

Activate the virtual environment.

    source env/bin/activate
    
Now use the `requirements.txt` to install all necessary dependencies via _PyPi_.
The `*` stands for `dev` or `prod`.

    pip install -r requirements_*.txt
    
You can now use the script `start_server.sh` to start a local server, 
as it will use exactly this environment `env`. Use a parameter named `DEV` 
to start this server in _Debug-Mode_. 

    sh start_server.sh DEV 

Dependencies can also be found in the `./requirements_dev.txt` file.
These can be downloaded and installed using the following command: 

`pip install <dependencyname>==<version>`

necessary Dependencies :

| Dependency | Version |
| :--------- | :------ |
| Flask      | 1.0.2   |
| openpyxl   | 2.5.3   |
| celery     | 4.2.0   |
| requests   | 2.19.1  |
| schedule   | 0.5.0   |
| SQLAlchemy | 1.3.6   |
| pandas     | 0.25.1  |
| psycopg2-binary | 2.8.3|

Environment variables: 

| Key | Value |
| :--------- | :------ |
| HUB_MAIN_URL      | URL to Hub Search API   |
| HUB_CATALOG_URI   | Filter to get all catalogues (works for piveau hub)   |
| HUB_DATASET_URI     | Filter to get details about a dataset (works for piveau hub)   |
| FILE_UPLOAD_API   | Hub API for file upload   |
| FILE_UPLOAD_AUTH   | Token for using upload service   |
| DIST_API | Hub API for adding distributions   |
| DIST_API_AUTH     | Token for using distribution API  |
| DB_USER | User of the PostgreSQL Database|
| DB_PASSWORD | Password of the PostgreSQL User|
| DB | Name of the database for use |
| DB_HOST | Name of the databaste host (in this case the name of the Docker service)|
| DB_PORT | Importing if not using the standard port (5432)|
| DB_IN_DOCKER | Boolean, if PostgreSQL Database is used with Docker. An external PostgreSQL Database can also be used. |